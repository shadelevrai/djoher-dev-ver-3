const fetch = require("node-fetch");

const ext = require('./libs/API.js')
const langage = require('./data/langage2.js')

exports.sourceNodes = async ({
  actions,
  createNodeId,
  createContentDigest
}) => {
  const {
    createNode
  } = actions;
  const response = await fetch("http://89.2.199.106:8880/api/djohers/all-article");
  const data = await response.json();
  for (const result of data) {
    const nodeId = createNodeId(`${result._id}`);
    const nodeContent = JSON.stringify(result);
    const node = Object.assign({}, result, {
      id: nodeId,
      originalId: result._id,
      parent: null,
      children: [],
      internal: {
        type: "ArticleJson",
        content: nodeContent,
        contentDigest: createContentDigest(result)
      }
    });
    createNode(node);
  }
};

exports.createPages = async ({
  actions
}) => {
  const {
    createPage
  } = actions

  const articles = await ext.API('http://89.2.199.106:8880/api/djohers/all-article')

  articles.forEach(data => {
    createPage({
      path: `/article/${data.titleLink}`,
      component: require.resolve(`./src/templates/article.js`),
      context: {
        data
      },
    })
  })

    langage.forEach(data=>{
    createPage({
      path:`/guide/${data.link}`,
      component:require.resolve('./src/templates/guide.js'),
      context : {
        data,
        articles
      }
    })
  })
}

// exports.createPages=async({
//   actions
// })=>{
//   const {createPage}=actions

//   langage.forEach(data=>{
//     createPage({
//       path:`/guide/${data.link}`,
//       component:require.resolve('./src/templates/guide.js'),
//       context : {
//         data
//       }
//     })
//   })
// }
