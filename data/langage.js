const langage = [
  {
      "name": "Node",
      "link": "node"
  }, {
      "name": "Mongo",
      "link": "mongo"
  }, {
      "name": "Socket.io",
      "link": "socketio"
  }, {
      "name": "React",
      "link": "react"
  }, {
      "name": "Angular",
      "link": "angular"
  }, {
      "name": "Vue",
      "link": "vue"
  }, {
      "name": "Git",
      "link": "git"
  },{
      "name": "CSS",
      "link": "css"
  },{
      "name": "Javascript",
      "link": "javascript"
  }
]

export default langage