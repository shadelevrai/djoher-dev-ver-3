import React from 'react'
import { useLocation } from '@reach/router';

import Layout from "../components/layout"
import ArticlePreviewList from "../components/articlePreviewList"

const Guide=()=>{
  const location = useLocation();
  console.log("Guide -> location", location.pathname)
  return(
    <Layout>
      <div className="columns">
            <div className="column">
                <h1 className="title has-text-centered">La liste des articles</h1>
                <ArticlePreviewList/>
            </div>
        </div>
    </Layout>
  )
}

export default Guide