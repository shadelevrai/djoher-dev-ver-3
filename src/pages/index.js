import React from "react"
import { useStaticQuery, graphql } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import TwitterWhoToFollow from "../components/twitterWhoToFollow"
import ArticlePreviewList from '../components/articlePreviewList'
import imageMe from '../images/seo.png'

import langage from '../../data/langage'

import '../styles/index.scss'

const IndexPage = () => {
//   const articleJson = useStaticQuery(graphql`
//   query {
//     allImage {
//       nodes {
//         id 
//       }
//     }
//   }
// `)
// console.log("ArticlePreviewList -> articleJson", articleJson)
  return(
  <Layout>
    <SEO title="Bienvenue sur Djoher-dev" />

    {/* <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image/>
    </div> */}
                <div className="index">
                <section>
                    <div className="columns">
                        <div className="column is-8 is-offset-2">
                            <div className="columns thebluediv">
                                <div className="column is-2 ml-5 mt-5">
                                    <a href="/me">
                                        <figure className='image'>
                                            <img src={imageMe} alt="" className="is-rounded" />
                                        </figure>
                                        <p className="has-text-centered has-text-grey is-family-secondary">Ryan Djoher</p>
                                    </a>
                                </div>
                                <div className="column is-9 mb-2 mt-2">
                                    <h1 className='title has-text-white is-size-4'>Astuces et tutoriels Javascript</h1>
                                    <h2 className="subtitle has-text-white is-size-5">Bienvenue sur Djoher-dev, le
                                        site qui va peut-être vous apprendre quelque chose</h2>
                                    {langage
                                    .map(({name,link}) => <div key={name}
                                        className='button is-success buttonLangage mt-1'>
                                        <a href={`guide/${link}`} className="has-text-weight-bold">{name}</a>
                                    </div>)}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <p className="is-size-6 mt-6 has-text-centered has-text-weight-semibold">Pour être à l'aise dans la
                    lecture
                    des articles, je vous conseille d'avoir des bases dans le HTML/CSS et Javascript</p>
                <div className="columns">
                    <div className="column is-4 is-offset-4">
                        <hr />
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-9 is-12-touch">
                        <ArticlePreviewList /*articlePreview={articlePreview}*/ />
                    </div>
                    <div className="column is-3 is-hidden-touch">
                        <TwitterWhoToFollow />
                    </div>
                </div>
            </div>
  </Layout>
  )
}

export default IndexPage
