import React, {useState} from "react"
import TwitterProfile from "./twitterProfile"

const TwitterWhoToFollow=()=>{
  const [profiles] = useState([{
    name: "Darius Foroux",
    imgLink: "/daruisImg.jpg",
    description: "Auteur",
    followLink: "https://twitter.com/dariusforoux",
    followName: "@dariusforoux"
  },{
    name: "Eddy Vinck",
    imgLink: "/eddyVinck.jpg",
    description: "Developpeur JS",
    followLink: "https://twitter.com/EddyVinckk",
    followName: "@EddyVinck"
  },{
    name: "Ania Kubow",
    imgLink: "/aniaKubow.jpg",
    description: "Software dev",
    followLink: "https://twitter.com/ania_kubow",
    followName: "@ania_kubow"
  },{
    name:"Javascript Kicks",
    imgLink: "/javascriptKicks.jpg",
    description: "Tips Javascript",
    followLink: "https://twitter.com/JavaScriptKicks",
    followName: "@JavaScriptKicks"
  },{
    name:"Dor Moshe",
    imgLink: "/dorMoshe.jpg",
    description: "Bloggeur",
    followLink: "https://twitter.com/DorMoshe",
    followName: "@DorMoshe"
  }])
  return(
    <div className="box pt-3">
      <h3 className="has-text-grey is-italic has-text-weight-semibold is-size-6 has-text-centered">Who to follow</h3>
      <div className="columns">
        <div className="column is-4 is-offset-4" style={{padding:'0'}}>
          <hr/>
        </div>
      </div>
      {profiles.map(item=> <div key={item.name}><TwitterProfile profile={item}/></div>)}
    </div>
  )
}

export default TwitterWhoToFollow