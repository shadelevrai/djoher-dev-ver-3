/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React from "react"
import { useStaticQuery, graphql } from "gatsby"

import Header from "./header"
import Footer from './footer'

import "../styles/layoutIndex.scss"

const Layout = ({ children }) => {

  const globalData = useStaticQuery(graphql`
  {
    allArticleJson {
      edges {
        node {
          intro
          langage
          originalId
          title
          readminutes
          titleLink
          category
          article {
            elementCategory
            value
          }
        }
      }
    }
    site {
      siteMetadata {
        title
      }
    }
  }
  `)

  return (
    <>
      <Header siteTitle={globalData.site.siteMetadata?.title || `Title`} />
      <div className="columns is-centered" id="layoutIndex">
        <div className="column is-narrow">
          <div className="space">
            <main>{children}</main>
          </div>
        </div>
      </div>
      <Footer/>
    </>
  )
}

export default Layout
