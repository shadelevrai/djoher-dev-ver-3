import React from 'react'
import '../styles/tags.scss'

const tags=({tags, size})=>{
  return(
    <div>
      {tags.map(tag => <span key={tag} className={`langageFrame tag is-success is-light is-${size}`}>
                <a href={`/guide/${tag}`}>
                    <span>{tag}</span>
                </a>
            </span>)}
    </div>
  )
}

export default tags