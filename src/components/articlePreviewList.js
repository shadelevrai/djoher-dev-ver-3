import React from 'react'
import { useStaticQuery, graphql } from "gatsby"

import ArticlePreview from './articlePreview'

const ArticlePreviewList=({nameTags=false})=>{

  const globalData = useStaticQuery(graphql`
  {
    allArticleJson {
      edges {
        node {
          langage
          originalId
          title
          readminutes
          titleLink
          category
          createDate
        }
      }
    }
  }
  `)

  return(
    <article>
      <div className="columns is-multiline articleList is-centered">
        {nameTags ?
        globalData.allArticleJson.edges.filter(article=>article.node.langage.find(tag=>tag===nameTags))
        .map(articlesTag=><div key={articlesTag.node.title} className="column">
          <ArticlePreview articlePreview={articlesTag.node} />
        </div>)
        :
        globalData.allArticleJson.edges
        .map(item => <div key={item.node.originalId} className="column is-narrow">
          <ArticlePreview articlePreview={item.node} />
        </div>)}
      </div>
    </article>
  )
}

export default ArticlePreviewList