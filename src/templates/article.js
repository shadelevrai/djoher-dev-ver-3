import React from "react"
import decode from 'unescape'

import Tags from "../components/tags"
import Signature from "../components/signature"
import Layout from "../components/layout"

import '../styles/article.scss'

export default function Langage({ pageContext: { data } }) {

  const {title, description, langage, intro, createDate, readminutes, article} = data

  function displayElement({elementCategory, value}) {
    if (elementCategory === 'subtitle') {
        return <p className='subtitle is-3 has-text-weight-bold'>{value}</p>
    }
    if (elementCategory === 'text') {
        return <p className="text is-1 has-text-weight-semibold">{value}</p>
    }
    if (elementCategory === 'code') {
        return <pre className="code has-text-weight-semibold">{decode(value)}</pre>
    }
    if (elementCategory === 'img') {
        return <figure className="columns is-centered">
            {/* eslint-disable-next-line no-undef */}
            <img
                className="image imgArticle"
                // eslint-disable-next-line no-undef
                src={`/article/${title}/${value}.png`} alt="image pour habiller article"></img>
        </figure>
    }
    if (elementCategory === 'redTitle') {
        return <h2 className='subtitle is-3 has-text-danger has-text-weight-bold'>{value}</h2>
    }
    if (elementCategory === 'list') {
        return <ol
            dangerouslySetInnerHTML={{
            __html: decode(value)
        }}></ol>
    }
    if (elementCategory === "link") {
        return <a href={value}>{value}</a>
    }
    if (elementCategory === "html") {
        return <div dangerouslySetInnerHTML={{__html:decode(value)}}></div>
    }
}

  return (
  <Layout>
            <h1 className="title has-text-centered is-size-2">{title}</h1>
    <section>
            <div className="article mb-6">
            <div className="columns is-centered tags">
                <Tags tags={langage} size={"medium"}/>
            </div>
            <figure className="columns is-centered">
                <img // eslint-disable-next-line no-undef
                    src={`/imgintro/${title}.png`} className="image imgIntro" alt={title}></img>
            </figure>
            <div className="columns">
                <div className="column is-8 is-offset-2">
                    <h2 className="is-size-5 has-text-centered has-text-weight-bold intro">{intro}</h2>
                    <div className="columns has-background-light signatureSpace">
                        <div className="column is-10 is-offset-1">

                            <Signature
                                articleDateCreate={createDate}
                                articleReadminutes={readminutes}/>
                        </div>
                    </div>
                    {article
                        .map((item,index) => <div key={index}> {displayElement(item) }</div>)}
                        <div className="columns has-background-light signatureSpace">
                        <div className="column is-10 is-offset-1">

                            <Signature
                                articleDateCreate={createDate}
                                articleReadminutes={readminutes}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </Layout >
  )
}