import React from 'react'

import Layout from "../components/layout"
import ArticlePreviewList from "../components/articlePreviewList"

import "../styles/mystyles.scss"

export default function Guide({pageContext:{data,articles}}){
  // console.log("Guide -> articles", articles)
  const {link,name} = data
  return(
    <Layout>
      <div className="columns">
        <div className="column">

      <h1 className="title has-text-centered">Liste des articles <span className="is-capitalized">{name}</span></h1>
        {/* {articles
        .filter(article=>article.langage.find(elem=>elem === link))
        .map(item=><div key={item.title}><ArticlePreviewList articlePreview={item}/></div>)
        } */}
        <ArticlePreviewList nameTags={link} />
        </div>
      </div>
    </Layout>
  )
} 