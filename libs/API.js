const fetch = require('isomorphic-fetch')

// export default async function API(link){
//   const res = fetch(link)
//   const data = await res.json()
//   return data
// }

module.exports = {
  API: async function (link) {
    const res = await fetch(link)
    const data = await res.json()
    return data
  }
}